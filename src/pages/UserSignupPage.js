import React from 'react'
import { curry, objOf } from 'ramda'

export class UserSignupPage extends React.Component {

  state = {
    displayName: '',
    username: '',
    password: '',
    passwordRepeat: '',
    pendingApiCAll: false
  }

  setStateWithEvent = (key, event) => this.setState(objOf(key, event.target.value))
  setStateBy = curry(this.setStateWithEvent)
  onChangeDisplayName = this.setStateBy('displayName')
  onChangeUserame = this.setStateBy('username')
  onChangePassword = this.setStateBy('password')
  onChangePasswordRepeat = this.setStateBy('passwordRepeat')

  onClickSignup = () => {
    const user = {
      username: this.state.username,
      displayName: this.state.displayName,
      password: this.state.password
    }
    this.setState({pendingApiCAll: true})
    this.props.actions.postSignup(user).then((response) => {
      this.setState({pendingApiCAll: false})
    })
  }

  render() {
    return (
      <div className="container">
        <h1 className="text-center">Sign Up</h1>
        <div className="col-12 mb-3">
          <label>Display Name</label>
          <input 
            className="form-control"
            placeholder="Your display name" 
            value={this.state.displayName}
            onChange={this.onChangeDisplayName}
          />
        </div>
        <div className="col-12 mb-3">
          <label>Username</label>
          <input 
            className="form-control"
            placeholder="Your username"
            value={this.state.username}
            onChange={this.onChangeUserame}
          />
        </div>
        <div className="col-12 mb-3">
          <label>Password</label>
          <input 
            className="form-control"
            placeholder="Your password"
            type="password"
            value={this.state.password}
            onChange={this.onChangePassword}
          />
        </div>
        <div className="col-12 mb-3">
          <label>Repeat Password</label>
          <input 
            className="form-control" 
            placeholder="Repeat your password" 
            type="password"
            value={this.state.passwordRepeat}
            onChange={this.onChangePasswordRepeat}
            />
        </div>
        <div className="text-center">
          <button className="btn btn-primary" onClick={this.onClickSignup}
            disabled={this.state.pendingApiCAll}>
              {this.state.pendingApiCAll && (
                <div className="spinner-border text-light spinner-border-sm mr-1 my-1" role="status">
                <span className="sr-only">Loading...</span>
                </div>
              )}
              <span>Sign Up</span>
          </button>
        </div>

      </div>
    )
  }
}

UserSignupPage.defaultProps = {
  actions: {
    postSignup: () => {
      new Promise((resolve, reject) => resolve({}))
    }
  }
}

export default UserSignupPage